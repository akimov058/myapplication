<html>
<head>
    <title>Intents</title>
</head>
<body>
<H1>Intents</H1>
<p>An Intent is a messaging object you can use to request an action
    from another app component.
    Although intents facilitate communication between components in several ways, there are three
    fundamental use-cases:</p>

<ul>
    <li><b>To start an activity:</b>
        <p>An Activity represents a single screen in an app. You can start a new
            instance of an Activity by passing an Intent
            to startActivity(). The Intent
            describes the activity to start and carries any necessary data.</p>

        <p>If you want to receive a result from the activity when it finishes,
            call startActivityForResult(). Your activity receives the result
            as a separate Intent object in your activity's onActivityResult() callback.
            For more information, see the Activities guide.</p></li>

    <li><b>To start a service:</b>
        <p>A Service is a component that performs operations in the background
            without a user interface. You can start a service to perform a one-time operation
            (such as download a file) by passing an Intent
            to startService(). The Intent
            describes the service to start and carries any necessary data.</p>

        <p>If the service is designed with a client-server interface, you can bind to the service
            from another component by passing an Intent to bindService(). For more information, see the Services guide.</p></li>

    <li><b>To deliver a broadcast:</b>
        <p>A broadcast is a message that any app can receive. The system delivers various
            broadcasts for system events, such as when the system boots up or the device starts charging.
            You can deliver a broadcast to other apps by passing an Intent
            to sendBroadcast(), sendOrderedBroadcast(), or sendStickyBroadcast().</p>
    </li>
</ul>
</body>
</html>